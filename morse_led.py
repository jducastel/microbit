# test d'ecriture de message en morse

# saisie des traits et des points avec les boutons A et B
# "pause" / validation avec A+B ? ou avec orientation du microbit ?
# stockage du message en mémoire
# revue / vérification du message via la grille LED
# "envoi" du message avec une LED RGB https://shop.pimoroni.com/products/rgb-led-for-micro-bit
# sélection du mode écriture / envoi / validation / correction avec detection de mouvement / orientation
# changement de couleur entre les lettres / mots pour aider à la lecture ?

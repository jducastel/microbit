import microbit
import radio

# https://www.firialabs.com/blogs/lab-notes/continuous-rotation-servos-with-python-and-the-micro-bit

class MicrobitCServo:
    """ Microbit continuous servo controller """
    
    def __init__(self, pin):
        self.pin = pin
        
    def clockwise(self):
        self.pin.write_analog(1023 * 1.0 / 20)
        
    def counterclockwise(self):
        self.pin.write_analog(1023 * 2.0 / 20)
        
    def stop(self):
        # self.pin.write_analog(0)
        self.pin.write_analog(1023 * 1.5 / 20)

class RingCar:
    
    def __init__(self):
        self.left_pin = microbit.pin1
        self.right_pin = microbit.pin2
        self.left = MicrobitCServo(self.left_pin)
        self.right = MicrobitCServo(self.right_pin)
        self.left.go = self.left.counterclockwise
        self.right.go = self.right.clockwise
        self.left.backward = self.left.clockwise
        self.right.backward = self.right.counterclockwise

    def forward(self):
        self.left.go()
        self.right.go()
        microbit.display.show(microbit.Image.ARROW_S)
        
    def backward(self):
        self.left.backward()
        self.right.backward()
        microbit.display.show(microbit.Image.ARROW_N)
    
    def stop(self):
        self.left.stop()
        self.right.stop()
        microbit.display.clear()
        
    def go_left(self):
        self.left.stop()
        self.right.go()
        microbit.display.show(microbit.Image.ARROW_E)
    
    def go_right(self):
        self.right.stop()
        self.left.go()
        microbit.display.show(microbit.Image.ARROW_W)
        
    def radio_controlled(self, channel=77):
        radio.config(channel=channel)
        radio.on()
        microbit.display.scroll('OK {}'.format(channel))
        while True:
            order = radio.receive()
            if order == 'forward':
                self.forward()
            elif order == 'left':
                self.go_left()
            elif order == 'right':
                self.go_right()
            if order == 'backward':
                self.backward()
            else:
                self.stop()

car = RingCar()
car.radio_controlled()
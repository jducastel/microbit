# test choix de picto en fonction de l'orientation
from microbit import *

# creation des images
good_img = Image("50005:90009:09990:00000:00000")
bad_img = Image("00000:00000:09990:90009:50005")
neutral_img = Image("00000:00000:00000:00000:0000")

emotion = "neutral"  # neutral / good / bad

# boucle infinie
while True:
    # definition de l'humeur a partir de l'accelerometre
    z = accelerometer.get_z()
    if z < 0:
        emotion = "good"
    elif z > 400:
        emotion = "bad"
    # affichage de la derniere humeur enregistree
    if emotion == "good":
        display.show(good_img)
    elif emotion == "bad":
        display.show(bad_img)

    sleep(200)

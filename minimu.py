# Ecrit ton programme ici ;-)
import microbit
import music

gamme = ['A','B','C','D','E','F','G']

numero_note = 0
pause = 300

def joue_note():
    microbit.display.show(gamme[numero_note])
    music.play(gamme[numero_note])
    microbit.sleep(pause)

while True:
    # gesture = microbit.accelerometer.current_gesture()
    if microbit.accelerometer.was_gesture('up') or microbit.accelerometer.was_gesture('down'):
        joue_note()
    elif microbit.accelerometer.was_gesture("left") and numero_note > 0:
        numero_note = numero_note - 1
        # microbit.sleep(pause)
    elif microbit.accelerometer.was_gesture("right") and numero_note < len(gamme) - 1:
        numero_note = numero_note + 1
        # microbit.sleep(pause)
    else:
        microbit.display.clear()
    microbit.sleep(10)


# emetteur radio microbit
import microbit
import radio

class RingCarRemote:
    
    def __init__(self, channel=77):
        self.channel = channel
        radio.config(channel=self.channel)
        radio.on()
        
    def control_with_buttons(self):
        microbit.display.scroll('boutons {}'.format(self.channel))
        while True:
            if microbit.button_a.is_pressed() and microbit.button_b.is_pressed():
                self.send_forward()
            elif microbit.button_a.is_pressed():
                self.send_left()
            elif microbit.button_b.is_pressed():
                self.send_right()
    
    def control_with_accelerometer(self):
        microbit.display.scroll('accel {}'.format(self.channel))
        while True:
            if microbit.accelerometer.is_gesture('down'):
                self.send_forward()
            if microbit.accelerometer.is_gesture('left'):
                self.send_left()
            if microbit.accelerometer.is_gesture('right'):
                self.send_right()
            if microbit.accelerometer.is_gesture('up'):
                self.send_backward()
    
    def send_left(self):
        radio.send('left')
        
    def send_right(self):
        radio.send('right')
        
    def send_forward(self):
        radio.send('forward')
        
    def send_backward(self):
        radio.send('backward')

remote = RingCarRemote()
remote.control_with_accelerometer()
